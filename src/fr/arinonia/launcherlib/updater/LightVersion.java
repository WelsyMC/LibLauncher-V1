package fr.arinonia.launcherlib.updater;

import fr.arinonia.launcherlib.launchlib.Launcher;
import fr.arinonia.launcherlib.updater.versions.CompleteVersion;
import fr.arinonia.launcherlib.updater.versions.ForgeVersion;


import java.net.URL;

public class LightVersion extends fr.arinonia.launcherlib.updater.versions.Version {
    private CompleteVersion completeVersion;
    private ForgeVersion forgeVersion;



    public void setId(String id) {
        super.setID(id);;
    }

    public void setUrl(URL url) {
        super.setUrl(url);
    }

    public void setType(fr.arinonia.launcherlib.updater.Version.VersionType type) {
        super.setType(type);
    }

    public void setForgeVersion(ForgeVersion forgeVersion) {
        this.forgeVersion = forgeVersion;
    }

    public ForgeVersion getForgeVersion() {
        return forgeVersion;
    }

    @Override
    public URL getUrl() {
        return super.getUrl();
    }

    public String getId() {
        return super.getId();
    }

    public fr.arinonia.launcherlib.updater.Version.VersionType getVersionType() {
        return super.getType();
    }

    public CompleteVersion getCompleteVersion() {
        return completeVersion;
    }

    public void setCompleteVersion(CompleteVersion completeVersion) {
        this.completeVersion = completeVersion;
    }


    @Override
    public Game update(Launcher launcher) {
        VersionDownloader downloader = new VersionDownloader(launcher);
        Game game  = downloader.downloadVersion(this, this.getCompleteVersion());
        downloader.getDownloadManager().check();
        downloader.getDownloadManager().startDownload();
        downloader.unzipNatives();
        downloader.getDownloadManager().check();

        return game;
    }


}
